import { merge } from "lodash";

const envConfigs = {
  local: {
    endpoint: "LocalEndpoint",
  },
  production: {
    endpoint: "prodEndpoint",
  },
};

const commonConfig = {
  common: "42",
};

console.log(process.env);

const appEnv = process.env.REACT_APP_ENV || "production";
const envConfig = envConfigs[appEnv];
const config = merge(commonConfig, envConfig);

export default config;
